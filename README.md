# Truemark Employee Handbook

Truemark Technology (Registered: Truemark Private Limited) is a remote first, quality focused company that helps entrepreneurs around the world build quality and impactful technology based solutions.

## We want Truemark to be a dream workplace for engineers. 
Our workplace should be pleasant and supportive and our engineers should take pride in:
1. Building solutions that are down to the last detail, have pleasant UI and Intuitive UX.
2. Help clients validate their ideas as early as possible.

This Handbook provides an overview of why we exist, our roles, our investment into staff welfare, our team norms, Software Development Life Cycle(SDLC) guides, and details of our processes, policies and benefits.

If you’re reading this just after joining the company, it’s particularly on you, actually. It’s harder for us slowly-boiled frogs who’ve been with Truemark for a while to spot the broken ways. Please take advantage of that glorious, shiny ignorance of being new, and question things.

If you're a visitor and want to use them, feel free to use them anywhere, but we'd appreciate that you linked us back in appreciation.

## Contributing to our Handbook

We're a company that loves to improve our process continuously, so we're going to need everyone to help us keep this up to date. If you encounter something that you think should be in this Handbook or spot a typo, please feel free to [open a pull request](https://github.com/TruemarkDev/handbook/pulls). If you have a question feel free to [open an issue](https://github.com/TruemarkDev/handbook/issues). This applies whether you're a Truemark employee or not :)

For more information on contributing, including how to as a non-technical user, read our [CONTRIBUTING.md](guides/CONTRIBUTION.md).

## Table of contents

### Introduction to Truemark Technology

* [About Truemark](company/about.md) – Why we do what we do
* [Welcome Pack](company/welcome_pack.md) – All you need to know when joining Truemark

### Roles

* [About Roles](roles/README.md) – Overview of our roles
* [All Roles](roles/) – Directory of roles

### Benefits & Perks

Balancing life and work:

* ✈️ [Flexible Holiday](benefits/flexible_holiday.md) – We trust you to take as much holiday as you need
* 🕰️ [Flexible Working Hours](benefits/working_hours.md) – We are flexible with what hours you work
* 🗓️ [Flexible Working Days](benefits/flexible_working.md) – We are flexible to the amount of days you work in a week
* 👶 [Flexible Parental Leave](guides/welfare/parental_leave.md) – We provide flexible parental leave options
* 👩‍💻 [Remote Working](benefits/remote_working.md) – We offer part-time remote working for all our staff
* 🤗 [Paid counselling](guides/welfare/paid_counselling.md) – We offer paid counselling as well as financial and legal advice
* 🏖️ [Paid anniversary break](benefits/paid_anniversary_break.md) – We celebrate your 3 and 5 year anniversary with us by buying your family a holiday
